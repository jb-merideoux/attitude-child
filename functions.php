<?php
/*
 * Remove the woocommerce sidebar and main content and replace it with our own.
 * Most of our changes are in the woocommerce folder in the child theme.
 */
remove_filter( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
remove_filter( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

remove_filter( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_filter( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_filter( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_filter( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_filter( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

remove_filter( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_filter( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_filter( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
remove_filter( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_filter( 'woocommerce_variable_add_to_cart', 'woocommerce_variable_add_to_cart', 30 );

add_filter( 'woocommerce_show_product_images', 'woocommerce_show_product_images', 5 );
add_filter( 'woocommerce_single_product_add_to_cart', 'woocommerce_template_single_add_to_cart', 5 );
add_filter( 'woocommerce_variable_add_to_cart', 'woocommerce_variable_add_to_cart', 5 );

add_filter('woocommerce_single_product_summary', 'show_single_product_details', 20);
add_filter('woocommerce_single_product_summary', 'woocommerce_product_description_tab', 30);

/*
 * Declare WooCommerce support 
 */ 
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_filter( 'after_setup_theme', 'woocommerce_support' );


/*
 * Load the JavaScript for every page
 */
function loadScript(){
	wp_enqueue_script('main', get_stylesheet_directory_uri() . '/library/js/main.js');

	if( is_page( 'gallery') ){
		add_thickbox();
		wp_enqueue_script('slideshow_links', get_stylesheet_directory_uri() . '/library/js/slideshow_links.js');
	}
	
	if( is_page('who is jbm?') ){
		add_thickbox();
		wp_enqueue_script('whoisjbm', get_stylesheet_directory_uri() . '/library/js/whoisjbm.js');
	}

	if( is_shop() || is_product_category() ){
		wp_enqueue_script('shop', get_stylesheet_directory_uri() . '/library/js/shop.js');
	}
}
add_filter('wp_enqueue_scripts', 'loadScript' );

/* Change the apperance of the nav bar */
function loadNavBar(){
	if( is_shop() || is_account_page() || is_cart() || is_checkout() || is_woocommerce() ){
		$page_title = "shop";
	}else{  // The home menu bar banner is used on 'home' and 'news'
		$page_title = the_title('','',false);
		if($page_title != 'who is jbm?' && $page_title != 'gallery') $page_title = "home";
		$page_title = preg_replace('/\W+/', '', $page_title);
	}

	$background_url = get_stylesheet_directory_uri() . "/images/{$page_title}_menubar.jpg";
	$background_style = "background: url('$background_url') no-repeat top center / 100% 100%";
	if ( has_nav_menu( 'primary' ) ) {
		$nav_menu_args = array(
				'theme_location'    => 'primary',
				'container'         => '',
				'items_wrap'        => '<ul class="root">%3$s</ul>'
		);
		echo '<nav id="access" class="clearfix" style="' . $background_style . '">
				<div class="container clearfix">';
		wp_nav_menu( $nav_menu_args );
		echo '</div><!-- .container -->
				</nav><!-- #access -->';
	}
	else {
		$page_menu_args = array(
				'menu_class'  => 'root',
				'show_home'	  => '1'
		);
		echo '<nav id="access" class="clearfix" style="' . $background_style . '">
				<div class="container clearfix">';
		wp_page_menu( $page_menu_args );
		echo '</div><!-- .container -->
				</nav><!-- #access -->';
	}
}
add_filter('load_nav_bar', 'loadNavBar');

/*
 * Content to be shown before the single product summary. This consists of a banner with
 * the shopping cart link.
 */
function show_single_product_before(){
	echo <<<EOS
<div id="heading_container">
   <h1 id="cart_heading"><a href="/cart" id="cart_link">shopping cart</a></h1>
</div>
EOS;
}
remove_filter( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
add_filter( 'woocommerce_before_single_product_summary', 'show_single_product_before', 20 );

/*
 Removes the actual text from the menu items so the text
in their backgrounds is clearly visible.
*/
function modifyMenuItems($menu, $args){
	if($menu === 'home')
		return 'news';
	else 
		return $menu;
}
add_filter('wp_nav_menu_items', 'modifyMenuItems', 10, 2);


function show_single_product_details(){
	wc_get_template( 'single-product/details.php' );
}

/*
 * Show the banner content after a single product.
 */
function show_single_product_after(){
	echo <<<EOS
<div id="single-product-after" class="banner"></div>
EOS;
}
add_filter( 'woocommerce_after_single_product_summary', 'show_single_product_after', 10);

/* Change the "Add to Cart" text*/
function woo_custom_cart_button_text() {

	return __( '- buy me -', 'woocommerce' );

}
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );