<?php
/**
 * Single Product details
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */
global $product;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="details">
	<div id="buyme"><?php do_action( 'woocommerce_variable_add_to_cart' ); ?></div>
	<?php do_action('woocommerce_product_thumbnails'); ?>
	<div id="categories">
		<div id="category_title">categories:</div>
		<?php echo $product->get_categories( ', ', '<div id="category_list">', '</div>' ); ?>
	</div>
</div>

