<?php
/**
 * Single Product title
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */
global $product;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="title">
	<?php 
		do_action('woocommerce_show_product_images'); 
		$child_category = get_the_terms(get_the_ID(), 'product_cat')[0]->name;
	?>
	<div class="text">
		<h2 class="product_category entry-category"><?php echo $child_category; ?></h2>
		<h1 itemprop="name" class="product_title entry-title"><?php the_title(); ?></h1>
		<h3 class="product_attribute">- <?php echo $product->get_attribute('size'); ?> -</h3>
	</div>
</div>

