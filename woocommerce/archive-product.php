<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		Spencer
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

require_once 'WCClasses.php';
global $wp_query;
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header('shop'); ?>

<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action('woocommerce_before_main_content');
		
		// The available lines.
		$linebuilder = new LineBuilder();
		$lines = $linebuilder->lines;
		
?>
<?php 

$has_parent = false;
$categories = LineBuilder::get_categories();
$category_name = $wp_query->query_vars['product_cat'];
foreach($categories as $category){
	if($category->slug === $category_name){
		break;
	}
	$category = null;
}

if(!empty($category->category_parent)){
	$has_parent = true;	
}

if (is_product_category() ) : 

$shirtTypeList = Design::get_attribute_terms('size');
?>
<?php endif; ?>
<div id="heading_container">
	  <h1>available prints</h1>
	<?php if ($has_parent) : // Only show the shirt type for categories with a parent ?>
	  <span id="gender_container">
	  	<?php 
	  		
	  		foreach($shirtTypeList as $shirtType){
	  			if(!empty($separator)){
	  				echo $separator;
	  			}
	  			$separator = ' - ';
	  			
	  			$attr_parts = explode(' ', $shirtType->name);
	  			$gender = $attr_parts[0];
	  			$size = $attr_parts[1]; 			 			
	  			$clean_gender = explode('-', $shirtType->slug)[0];
	  			echo "<a href='#$clean_gender'>$gender</a>";  			
	  		}
	  	?>    
	  </span>
   <?php endif; ?>
	</div>
		<?php if ( have_posts() && !is_shop() ) : ?>	
	
	
					<?php
						/**
						 * woocommerce_before_shop_loop hook
						 *
						 * @hooked woocommerce_result_count - 20
						 * @hooked woocommerce_catalog_ordering - 30
						 */
						do_action( 'woocommerce_before_shop_loop' );
					?>
		
					<?php woocommerce_product_loop_start(); ?>
		
						<?php woocommerce_product_subcategories(); ?>
		
						<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>
		
					<?php woocommerce_product_loop_end(); ?>
		
				<?php elseif( !have_posts() && !is_shop() ):
						/*
						 * Show this for lines and designs that don't
						 * have products.
						 */						
						if($category){
							if(!$has_parent){
								foreach($lines as $ln){
									if($ln->term_id === $category->term_id){
										$ln->view('shop');
										break;
									}
								}
							} else {
								Design::defaultView();
							}
						}
						//var_dump($category);
				?>	
		
				<?php endif; 
				
				if (!is_product_category() ) : ?>

	<div id="top">
		<p id="blurb">jbm monotypes are unique pieces of wearable art&#8230;designed, printed and individualized by<br />
		the jbm team.  each design is printed on a total of 50 pieces of apparel before being retired.<br />
		every print is hand-modified using bleach, ink or thread, creating a one-of-a-kind &#8216;mono-type&#8217;.<br />
		monotypes are individually numbered and catalogued to ensure no two are rendered alike.</p>	
		<ul id="lineselection">
			<h1>available lines&#8230;</h1>
			<?php
			
			/* // For development only
			echo '<div id="thedump">';
			var_dump($lines);
			echo '</div>'; */
			
			?>
		</ul>
		<?php 
			if(LineBuilder::$isStoreClosed){
		?>
			<div id="comingsoon">
				<h1>Coming Soon</h1>
			</div>
		<?php 	
			}
		?>
		
	</div>

	<?php 
	foreach($lines as $ln){
		$ln->view('shop');
	}
		
	?>

	<?php
		endif;
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action('woocommerce_after_main_content');
	?>

<?php get_footer('shop'); ?>