<?php 
class Line{
	public $name = '';
	
	public $ulid = '';
	public $term_id;
	
	public $description = '';
	
	/**
	 * The designs in this line
	 * @var array
	 */
	public $designs = array();
	
	public function __construct(WP_Term $category){		
		$this->name = $category->name;
		$this->ulid = $this->name . 'lines';
		$this->term_id = $category->term_id;
		$this->description = $category->description;
	}
	
	public function addDesign(WP_Term $category){		
		$design = new Design($category);		
		
		if($category->parent === $this->term_id){			
			$this->designs[] = $design;
		}
	}
	
	/**
	 * Output an HTML view of this Line
	 * @param string $viewtype
	 */
	public function view($viewtype){
		switch($viewtype){
			case 'shop':
				$lineid = str_replace(" ", "", $this->name);
				$line_url = str_replace(' ', '-', $this->name);
				echo <<< EOS
	<div id="{$this->name}_row" class="linebanner $lineid">
		<ul id="{$this->ulid}">
 			<h1 id="$lineid"><a href="/shop/$lineid"></a></h1>
 			<h2>- {$this->description} -</h2>
EOS;
				$count  = 0;
				$items_per_row = 3;
				$lis = '';
				foreach($this->designs as $design){
					$designid = str_replace(" ", "", $design->name);
					$design_url = str_replace(' ', '-', $design->name);
					
					$lis .= <<< EOS
			<li>
				<a id="$designid" href="/shop/$line_url/$design_url">
					<img class="image" src="{$design->imgSrc}" />
				</a>
			</li>
EOS;
					if($count % $items_per_row === 0 && $count !== 0){
						echo <<< EOS
			<div class="design-row">
				$lis
			</div>
EOS;
						$lis = '';
												
					}
					$count++;
				}
				
				echo <<< EOS
			<div class="design-row">
				$lis
			</div>
EOS;
				
				echo <<< EOS
		</ul>
	</div>
	<div class="bottom"></div>
EOS;
			break;
			case 'shopnav':
				$lineid = str_replace(" ", "", $this->name);				
				echo "<li><a href='#$lineid'>{$this->name}</a></li>";
			break;
				
		}
	}
}

/**
 * A product design that belongs to a Line.
 * @author Spencer Williams
 *
 */
class Design{
	public $name = '';
	public $term_id;
	
	public $imgSrc = '';
	
	public $description = '';
	
	public function __construct(WP_Term $category){
		$this->name = $category->name;
		$this->term_id = $category->term_id;
		$this->description = $category->description;
		
		$thumbnail_id = get_woocommerce_term_meta($this->term_id, 'thumbnail_id', true);
		$this->imgSrc = wp_get_attachment_url($thumbnail_id);
	}
	
	/**
	 * An indexed array of WooCommerce product attribute names as the keys and HTML list item strings for the products of 
	 * that attribute as the values.
	 * @var array
	 */
	public static $attributeList = [];
	
	/**
	 * Get a list of terms for this attribute. The returned array is result of calling get_terms()
	 * on the taxonomy name of the attribute.
	 * 
	 * @param string $attribute
	 * @return array
	 */
	public static function get_attribute_terms($attribute = ''){
		$wc_attributes = wc_get_attribute_taxonomies();
		
		foreach($wc_attributes as $wc_attribute){
			if($wc_attribute->attribute_name === $attribute){
				break;
			}
		}
		
		$wc_attribute_taxonomy_name = wc_attribute_taxonomy_name($wc_attribute->attribute_name);
		/*
		 * Only return the top level attributes
		 */
		return get_terms($wc_attribute_taxonomy_name, ['hierarchical' => true, 'parent' => 0]);
	}
	
	/**
	 * Output a default HTML view for designs with no products
	 */
	public static function defaultView(){
		$imgdir = '/wp-content/themes/attitude-child/images';
		echo <<< EOS
	<div class="design linebanner">
		<h1>available sizes&#8230;</h1>
		<div id="products">
		<div class="product" id="as04">
		<img src="$imgdir/products/all season 4.jpg">
		</div>
		<div class="product" id="as08">
		<img src="$imgdir/products/all season 8.jpg">
		</div>
		</div>
	</div>
EOS;
	}
}

/**
 * Create an array of product lines
 * @author Spencer Williams
 *
 */
class LineBuilder{
	public $lines = array();
	
	public static $isStoreClosed = false;
	
	/**
	 * Obtain Line and Design information from the WooCommerce categories
	 */
	public function __construct(){
		$categories = self::get_categories();
		
		// First, find out which categories are lines, which have a parent of 0
		foreach($categories as $category){
			if($category->parent === 0){
				$this->lines[] = new Line($category);
			} else {
				foreach($this->lines as $line){
					if($line->term_id === $category->parent){
						$line->addDesign($category);
					}
				}
			}
		}	
	}
	
	/**
	 * Get the WordPress categories for the product lines and designs
	 * 
	 *  @return array
	 */
	public static function get_categories(){
		return get_categories('taxonomy=product_cat&hide_empty=0');
	}
}
?>