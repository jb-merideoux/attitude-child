<?php
/**
 * Product Loop End
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
require_once get_stylesheet_directory() . '/woocommerce/WCClasses.php';

/*
 * Get a list of products in a collection with the gender type as keys and the
 * gender sizes as values.
 */
$shirtTypes = Design::$attributeList;

foreach($shirtTypes as $gender => $genderSizes){

	$gender_slug = sanitize_title($gender);
?>
<div class="gender_div" id="<?php echo $gender_slug . '_div'; ?>">
	<h1 id="<?php echo $gender_slug; ?>"><?php echo $gender; ?></h1>
<?php 
	foreach($genderSizes as $size => $products){
?>
		<?php echo $products; ?>
<?php 
	} // End foreach($genderSizes as $size => $products)
?>
</div>
<?php } // End foreach($shirtTypes as $gender => $genderSizes)
?>
</ul>
<div id="emailMessage">
	<p>Don't see your size? <a href="mailto:jbm@jbmerideoux.com">Email us!</a></p>
</div>