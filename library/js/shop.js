jQuery(function($) {
	var activeLink = $('a[href="#mens"]').addClass('active');
	var nonActiveLink = $('#gender_container a:not(.active)');
	
	if(nonActiveLink.length !== 0){
		var shirtType = nonActiveLink.attr('href').substring(1);
		$('#' + shirtType + '_div').hide();
	}
	
	var genderContainerLinks = $('#gender_container a');
	  
	genderContainerLinks.click(function(){
		var prevActiveLink = $('#gender_container a.active');
		var prevActiveDivName = prevActiveLink.attr('href').substring(1);
		var nextActiveDivName = $(this).attr('href').substring(1);
		
		if(prevActiveDivName === nextActiveDivName){
			return;
		}
		
		$('#' + prevActiveDivName + '_div').hide();
		$('#' + nextActiveDivName + '_div').show();
		
		prevActiveLink.removeClass('active');
		$(this).addClass('active');
	});
});