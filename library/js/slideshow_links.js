document.addEventListener("DOMContentLoaded", function(){
	var galleryImages = document.querySelectorAll(".slideshow-slide img");

	Array.prototype.forEach.call(galleryImages, function(image){
		var a = document.createElement("a");
		a.href = image.src;
		
		var text = image.parentNode.querySelector(".slideshow-slide-caption").textContent;
		
		a.setAttribute("class", "thickbox");
		a.setAttribute("rel", "people");
		a.setAttribute("title", text);
		
		var clone = image.cloneNode();
		a.appendChild(clone);

		image.parentNode.replaceChild(a, image);
	});
});