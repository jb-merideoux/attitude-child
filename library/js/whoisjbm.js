jQuery(function($){	
	$('.wp-caption a').each(function(index, element){
		$(this).addClass('thickbox');
		$(this).attr('rel', 'places');
		$(this).attr('title', $(this).find('img').attr('alt'));
	});
	
	/*
	 * Move the Thickboxes with the bios up
	 */
	var target = document.querySelector('body');
	var observer = new MutationObserver(function(mutations) {
		$.each(mutations, function(index, mutation){
			if(mutation.attributeName === 'class'){				
				if($(mutation.target).hasClass('modal-open')){
					function moveBox(){
						var tbCaption = $('#TB_caption:contains("Max Geimer"), #TB_caption:contains("Jonathan Applegate")'),
						tbWindow;
						
						if(tbCaption.length !== 0){
							tbWindow = $('#TB_window');
							tbWindow.css('top', '25%');
						}
						
						if($('#TB_Image[src*="thumb"]').length !== 0){
							tbWindow.addClass('small_TB_window');
						}
						
					}
					window.setTimeout(moveBox, 50);
				}
				return false;
			}			
		});
	});
	var config = { attributes: true };
	observer.observe(target, config);
});